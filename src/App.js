import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button, Container, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';

const data = [

  { id: 1, Nombres: "Bernardo Alfonso", Apellidos: "Serna Valencia" },
  { id: 2, Nombres: "Dayana", Apellidos: "Serna Restrepo" },
  { id: 3, Nombres: "Estefania", Apellidos: "Gallego" },
  

];


class App extends React.Component {

  state = {

    data: data,
    form: {
      id: '',
      Nombres: '',
      Apellidos: ''
    },
    modalInsertar: false,
    modalEditar: false,
  };

  handleChange = e => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,

      }

    });


  }
  mostrarModalInsertar = () => {

    this.setState({ modalInsertar: true });


  }
  ocultarModalInsertar = () => {

    this.setState({ modalInsertar: false });


  }


  mostrarModalEditar = (registro) => {

    this.setState({ modalEditar: true, form: registro });


  }
  ocultarModalEditar = () => {

    this.setState({ modalEditar: false });


  }


  insertar = () => {

    var valorNuevo = { ...this.state.form };
    valorNuevo.id = this.state.data.length + 1;
    var lista = this.state.data;
    lista.push(valorNuevo);
    this.setState({ data: lista, modalInsertar: false });

  }

  editar = (dato) => {

    var contador = 0;
    var lista = this.state.data;
    lista.map((registro) => {
      if (dato.id == registro.id) {

        lista[contador].Nombres = dato.Nombres;
        lista[contador].Apellidos = dato.Apellidos;
      }
      contador++;

    });

    this.setState({ data: lista, modalEditar: false });

  }

  eliminar = (dato) => {

    var opcion = window.confirm("Realmente desea eliminar el registro" + dato.id);
    if (opcion) {
      var contador = 0;
      var lista = this.state.data;
      lista.map((registro) => {
        if (dato.id == registro.id) {

          lista.splice(contador, 1);

        }
        contador++;

      });


      this.setState({ data: lista });
    }
  }

  render() {
    return (
      <>
        <Container>
          <br />

          <Button color="success" onClick={() => this.mostrarModalInsertar()}> Insertar Nuevo Usuario  </Button>

          <br /> <br />
          <Table>

            <thead><tr><th>ID</th>
              <th>NOMBRES</th>
              <th>APELLIDOS</th>
            </tr></thead>

            <tbody>

              {this.state.data.map((elemento) => (
                <tr>
                  <td>{elemento.id}</td>
                  <td>{elemento.Nombres}</td>
                  <td>{elemento.Apellidos}</td>
                  <td><Button color="primary" onClick={() => this.mostrarModalEditar(elemento)} >Editar</Button>{"  "}
                    <Button color="danger" onClick={() => this.eliminar(elemento)}  >Eliminar</Button></td>
                </tr>

              ))}

            </tbody>



          </Table>

        </Container>
        <Modal isOpen={this.state.modalInsertar}>

          <ModalHeader>
            <div>

              <h1> Insertar Registro</h1>

            </div>


          </ModalHeader>

          <ModalBody>

            <formGroup>
              <label>id:</label>
              <input className="form-control" readOnly type="text" value={this.state.data.length + 1} />
            </formGroup>

            <formGroup>
              <label>Nombres:</label>
              <input className="form-control" name="Nombres" type="text" onChange={this.handleChange} />
            </formGroup>

            <formGroup>
              <label>Apellidos:</label>
              <input className="form-control" name="Apellidos" type="text" onChange={this.handleChange} />
            </formGroup>

          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.insertar()}>Insertar</Button>
            <Button color="danger" onClick={() => this.ocultarModalInsertar()}>Cancelar</Button>


          </ModalFooter>

        </Modal>

        <Modal isOpen={this.state.modalEditar}>

          <ModalHeader>
            <div>

              <h1> Editar Registro</h1>

            </div>

          </ModalHeader>

          <ModalBody>

            <formGroup>
              <label>id:</label>
              <input className="form-control" readOnly type="text" value={this.state.form.id} />
            </formGroup>

            <formGroup>
              <label>Nombres:</label>
              <input className="form-control" name="Nombres" type="text" onChange={this.handleChange} value={this.state.form.Nombres} />
            </formGroup>

            <formGroup>
              <label>Apellidos:</label>
              <input className="form-control" name="Apellidos" type="text" onChange={this.handleChange} value={this.state.form.Apellidos} />
            </formGroup>

          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => this.editar(this.state.form)} >Editar</Button>
            <Button color="danger" onClick={() => this.ocultarModalEditar()}>Cancelar</Button>

          </ModalFooter>

        </Modal>
      </>
    );
  }

}

export default App;
